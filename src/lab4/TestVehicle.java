package lab4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import lab2.DumpTruck;
import lab2.SchoolBus;

/**
 * @author Lawrence
 * Route vehicles and manage car dealership inventory
 */
public class TestVehicle {				
	private static int[][] adjMatrix = new int[12][12];		// 2D array of adjacency matrix representation of graph
	private static DecimalFormat dec = new DecimalFormat(".##"); // For formatting decimal numbers
	
	public static void main(String[] args) {
		// instantiation of JFrame object
		JFrame frame = new JFrame("InputDialog1");	
		boolean loop = true;
		
		// Read text files and save to array
		String filepath = "src/lab4/graph.txt";
		copyTextFileIntoArray(filepath);
		
		// Instantiate DumpTruck and SchoolBus objects
		DumpTruck dt = new DumpTruck("Mack", "Granite", 2017, "White", 5.5, 13, 0);
		SchoolBus sb = new SchoolBus("Blue Bird", "Conventional", 2012, "Yellow", 11.2, 24, 0);
		
		// User Menu
		do {
			String choice = JOptionPane.showInputDialog(frame, "Choose and option by entering corresponding number\n"
														+ "1. Route school bus and dump truck\n"
														+ "2. Manage dealership family car inventory\n"
														+ "3. Exit program");
			if (choice.equals("1")){
				double dtSpeed = Double.parseDouble(JOptionPane.showInputDialog(frame, "enter average speed of Dump Truck"));
				double sbSpeed = Double.parseDouble(JOptionPane.showInputDialog(frame, "enter average speed of School Bus"));
				dt.setSpeed(dtSpeed);
				sb.setSpeed(sbSpeed);
				routeVehicles(dt, sb);	
			}else if (choice.equals("2")){
				double balance = Double.parseDouble(JOptionPane.showInputDialog(frame, "Enter dealership's account balance"));
				manageInventory(balance);
			}else if (choice.equals("3")){
				System.exit(0);
			}else{
				JOptionPane.showMessageDialog(null, "Invalid input");
				loop = true;
			}
		} while (loop);
		
	}
	
	/***
	 * Manage Dealership's inventory and execute transactions
	 */
	public static void manageInventory(Double capital){
		ArrayList<FamilyCar> fcars = new ArrayList<FamilyCar>();
		ArrayList<String[]> info = new ArrayList<String[]>();
		try {
			BufferedReader reader =new BufferedReader(new FileReader("src/lab4"
					+ "/inventory.csv"));
			String line = reader.readLine();	// skip header
			while((line=reader.readLine())!=null){
				String [] values =line.trim().split(",");
                FamilyCar fc = new FamilyCar(values[0], values[1], Integer.parseInt(values[2]), values[3], 
                		Double.parseDouble(values[4]), Double.parseDouble(values[5]), Double.parseDouble(values[6]));
                fcars.add(fc);
                String[] data = {values[7],values[8]," "};
                info.add(data);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double balance = capital;
		double cost;
		int sell=0, buy=0, pass=0;
		for (int c=0; c<fcars.size(); c++){
			if (info.get(c)[1].equals("Sell")){
				cost = getCostPrice(fcars.get(c), Integer.parseInt(info.get(c)[0]));
				balance += cost + (cost*0.15);
				info.get(c)[2] = "remove";
				sell += 1;
			} else if (info.get(c)[1].equals("Buy")){
				cost = getCostPrice(fcars.get(c), Integer.parseInt(info.get(c)[0]));
				if (cost <= balance){
					balance -= cost;
					info.get(c)[2] = "add";
					buy += 1;
				} else {
					pass += 1;
					info.get(c)[2] = "remove";
				}
			}
		}
		
		
		String summary = "Vehicles still owned by dealership after transactions are:\n";
		for (int c=0; c<fcars.size(); c++){
			if (info.get(c)[2].equals("add"))
				summary += fcars.get(c).toString()+"\n";
		}
		summary += sell+"  cars were sold by dealership\n";
		summary += buy+"  cars were purchased by dealership\n";
		summary += pass+"  deals were missed by dealership\n";
		summary += "Dealership's account balance after transactions is $"+dec.format(balance);
		summary += "\nDealership made a profit of $"+dec.format(balance-capital);
		JOptionPane.showMessageDialog(null,summary);
        
	}
	
	/***
	 * computes the cost price of a vehicle
	 * @param fc: FamilyCar object
	 * @param value: net value of vehicle
	 * @return value: computed gross value of vehicle
	 */
	public static double getCostPrice(FamilyCar fc, int value){
		if (fc.getColor().equalsIgnoreCase("Black"))
			value += 700;
		if (fc.getColor().equalsIgnoreCase("White"))
			value += 1000;
		value += 5000/(2018-fc.getYear());
		value += 49*fc.getTrunkcap();
		return value;
	}
	
	/***
	 * Route vehicles
	 */
	public static void routeVehicles(DumpTruck dtk, SchoolBus sbs){
		JFrame frame = new JFrame("InputDialog1");	
		int dtStart = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to start "
				+ "location of Dump Truck\n"+ displayNodeOptions(-1, -1, -1)));
		int dtEnd = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to destination "
				+ "of Dump Truck\n"+ displayNodeOptions(dtStart, -1, -1)));
		int sbStart = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to start "
				+ "location of School Bus\n"+ displayNodeOptions(dtStart, dtEnd,-1)));
		int sbEnd = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to destination "
				+ "of School Bus\n"+ displayNodeOptions(dtStart, dtEnd, sbStart)));
		int method1 = Integer.parseInt(JOptionPane.showInputDialog(frame, "choose graph travesal method for Dump Truck \n"
				+ "1. DFS\n2. Reccursion"));
		int method2 = Integer.parseInt(JOptionPane.showInputDialog(frame, "choose graph travesal method for School Bus \n"
				+ "1. DFS\n2. Reccursion"));
		String dtRoute = null, sbRoute = null;
		int[][] visited;
		
		if (method1==1) dtRoute = dfs(dtStart-1, dtEnd-1);
		else dtRoute = traverse(dtStart-1, dtEnd-1, visited = new int[12][12]);
		if (method2==1) sbRoute = dfs(sbStart-1, sbEnd-1);
		else sbRoute = traverse(sbStart-1, sbEnd-1, visited = new int[12][12]);
		
		boolean collision = false;
		char[] junctions = {'I','J','K','L'};
		
		do {
			// check for collision when both routes cross the same junction
			collision = false;
			
			for (char junction : junctions ){
				if (dtRoute.indexOf(junction)>=0 && sbRoute.indexOf(junction)>=0){ 
					int dtMilesToJunction = getMiles(dtStart-1, junction, dtRoute);
					int sbMilesToJunction = getMiles(sbStart-1, junction, sbRoute);
					double dtTime = (float)dtMilesToJunction/(float)dtk.getSpeed();
					double sbTime = (float)sbMilesToJunction/(float)sbs.getSpeed();
					if (dtTime == sbTime)
						collision = true;
				}
			}
			// reroute or change vehicle speed if collision
			if (collision){		
				String vehicle = JOptionPane.showInputDialog(frame, "A collision is pending, avoid collision by"
						+ " changing the speed of a vehicle\nChoose vehicle to control:\n"
						+ "1. Dump Truck\n"
						+ "2. School Bus");
				double speed = Double.parseDouble(JOptionPane.showInputDialog(frame, "Enter new average speed"));
				if (vehicle == "1")
					dtk.setSpeed(speed);
				else if (vehicle == "2")
					sbs.setSpeed(speed);
			} else{
				int sbMiles = getMiles(sbRoute);
				int dtMiles = getMiles(dtRoute);
				String summary = "School Bus route is: "+sbRoute
						+"\nTotal distance traveled is: "+sbMiles+" Miles"
						+"\nAverage speed during trip is: "+sbs.getSpeed()+" MPH"
						+"\nTotal time of trip is: "+dec.format(60*(sbMiles/sbs.getSpeed()))+" minutes"
						+"\nGas burned during trip is: "+dec.format(sbMiles/sbs.getMpg())+" gallons\n"
						+"\nDump Truck route is: "+dtRoute
						+"\nTotal distance traveled is: "+dtMiles+" Miles"
						+"\nAverage speed during trip is: "+dtk.getSpeed()+" MPH"
						+"\nTotal time of trip is: "+dec.format(60*(dtMiles/dtk.getSpeed()))+" minutes"
						+"\nGas burned during trip is: "+dec.format(dtMiles/dtk.getMpg())+" gallons\n";
				JOptionPane.showMessageDialog(null,summary);
			}
		} while (collision);
	}
	
	public static int getMiles(String route){
		int miles = 0;
		char[] rNodes = route.toCharArray();
		for (int c=0; c<rNodes.length-1; c++){
			miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
		}
		return miles;
	}
	
	public static int getMiles(int sin, char junc, String route){
		int miles = 0;
		char[] rNodes = route.toCharArray();
		if (sin == convertToInt(route.charAt(0))){
			for (int c=0; c<route.indexOf(junc)-1; c++){
				miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
			}
		} else {
			for (int c=route.indexOf(junc); c<route.length()-1; c++){
				miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
			}
		}
		return miles;
	}
	
	// recursive method to find a route from source to sink node
	public static String traverse(int src, int sink, int[][] visitedEdges){
		String des, route=null;
		int child = getUnvisitedChildNode(src, visitedEdges[src]);
		// Base Case
		if (child == -1){	// Leaf node
			if (src == sink) route = convertToString(src);
			return route;
		} else {
			do{
				visitedEdges[src][child] = 1;	// mark edge as visited
				visitedEdges[child][src] = 1;	// mark edge as visited
				des = traverse(child, sink, visitedEdges);
				if (des != null)
					return convertToString(src) + des;
				child = getUnvisitedChildNode(src, visitedEdges[src]);
			} while (child != -1);
			return route;
		}	
	}
	
	// DFS uses Stack data structure to find a route from source to sink node
	public static String dfs(int src, int sink)
	{
		String route="";	//Reset
		int node, child;
		int[][] visitedEdges = new int[12][12];
		Stack<Integer> s = new Stack<Integer>();
		boolean notfound = true;
		s.push(src);
		
		while (!s.isEmpty() && notfound)
		{
			node = (int) s.peek();
			child = getUnvisitedChildNode(node, visitedEdges[node]);
			if (child == sink){					// found route
				s.push(child);
				notfound = false;
			} else if (child >= 0){				// found unvisited child node
				visitedEdges[node][child] = 1;	// mark edge as visited
				visitedEdges[child][node] = 1;	// mark edge as visited
				if(s.search(child) == -1)	 	// if child node is not in stack
					s.push(child);
			} else s.pop();						// all child nodes are visited						
		}
		
		if (!notfound){
			while (!s.isEmpty())
				route = convertToString(s.pop()) + route;
		}
		return route;
	}
	
	public static int getUnvisitedChildNode(int parentNode, int[] visited) {
		int edge;
		for (int col=0; col<adjMatrix.length; col++) {
			edge = adjMatrix[parentNode][col];
			if (edge != 0 && visited[col] == 0) {
				return col;
			}
		}
		return -1;
	}
	
	/***
	 * Converts node index to alphabet
	 * @param node: integer representing node
	 * @return alphabet corresponding to node
	 */
	public static String convertToString(int node){
		switch (node){
		case 0: return "A";
		case 1: return "B";
		case 2: return "C";
		case 3: return "D";
		case 4: return "E";
		case 5: return "F";
		case 6: return "G";
		case 7: return "H";
		case 8: return "I";
		case 9: return "J";
		case 10: return "K";
		case 11: return "L";
		}
		return " ";
	}
	
	/***
	 * Converts node alphabet to integer
	 * @param node: alphabet representing node
	 * @return integer corresponding to node
	 */
	public static int convertToInt(char node){
		switch (node){
		case 'A': return 0;
		case 'B': return 1;
		case 'C': return 2;
		case 'D': return 3;
		case 'E': return 4;
		case 'F': return 5;
		case 'G': return 6;
		case 'H': return 7;
		case 'I': return 8;
		case 'J': return 9;
		case 'K': return 10;
		case 'L': return 11;
		}
		return -1;
	}
	
	/***
	 * lists only nodes that haven't yet been chosen
	 * @param node1, node2, node3 are already chosen nodes
	 * @return string listing available options
	 */
	public static String displayNodeOptions(int node1, int node2, int node3){
		String options = "";
		if (node1 != 1 && node2 != 1 && node3 != 1)
			options += "1. A\n";
		if (node1 != 2 && node2 != 2 && node3 != 2)
			options += "2. B\n";
		if (node1 != 3 && node2 != 3 && node3 != 3)
			options += "3. C\n";
		if (node1 != 4 && node2 != 4 && node3 != 4)
			options += "4. D\n";
		if (node1 != 5 && node2 != 5 && node3 != 5)
			options += "5. E\n";
		if (node1 != 6 && node2 != 6 && node3 != 6)
			options += "6. F\n";
		if (node1 != 7 && node2 != 7 && node3 != 7)
			options += "7. G\n";
		if (node1 != 8 && node2 != 8 && node3 != 8)
			options += "8. H";
		return options;
	}
	
	/***
	 * Read data from text files using buffered Reader and also prints content to console
	 * @param filepath: path to text file
	 */
	public static void copyTextFileIntoArray(String filepath) {
		try {		
			Scanner line = new Scanner(new File(filepath));
			int row = 0;
			int col;
			    
		    while (line.hasNextLine()) {
		    	col = 0;
		        Scanner word = new Scanner(line.nextLine());
		        while (word.hasNext()) {
		        	String s = word.next();
		        	adjMatrix[row][col] = Integer.parseInt(s);
		        	System.out.print(adjMatrix[row][col]+"\t");
		            col++;
		        }
		        row++;
		        System.out.println();
		        word.close();
		    }
		    line.close();			    
				    
	    } catch (IOException e) {
	    	e.printStackTrace();
		}
	}
}
