package lab3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import lab2.DumpTruck;
import lab2.SchoolBus;

/**
 * @author Lawrence
 * Route vehicles and manage car dealership inventory
 */
public class TestVehicle {
	private static int[][] adjMatrix = new int[12][12];		// 2D array of adjacency matrix representation of graph
	private static String[][] routeDict = new String[28][3];// 2D array for all routes
	private static DecimalFormat dec = new DecimalFormat(".##"); // For formatting decimal numbers
	
	public static void main(String[] args) {
		// instantiation of JFrame object
		JFrame frame = new JFrame("InputDialog1");	
		boolean loop = true;
		
		// Read text files and save to array
		String filepath = "src/lab3/graph.txt";
		copyTextFileIntoArray(filepath);
		filepath = "src/lab3/routes.txt";
		buildDictionary(filepath);
		
		// Instantiate DumpTruck and SchoolBus objects
		DumpTruck dt = new DumpTruck("Mack", "Granite", 2017, "White", 5.5, 13, 0);
		SchoolBus sb = new SchoolBus("Blue Bird", "Conventional", 2012, "Yellow", 11.2, 24, 0);
		
		// User Menu
		do {
			String choice = JOptionPane.showInputDialog(frame, "Choose and option by entering corresponding number\n"
														+ "1. Route school bus and dump truck\n"
														+ "2. Manage dealership family car inventory\n"
														+ "3. Exit program");
			if (choice.equals("1")){
				double dtSpeed = Double.parseDouble(JOptionPane.showInputDialog(frame, "enter average speed of Dump Truck"));
				double sbSpeed = Double.parseDouble(JOptionPane.showInputDialog(frame, "enter average speed of School Bus"));
				dt.setSpeed(dtSpeed);
				sb.setSpeed(sbSpeed);
				routeVehicles(dt, sb);	
			}else if (choice.equals("2")){
				double balance = Double.parseDouble(JOptionPane.showInputDialog(frame, "Enter dealership's account balance"));
				manageInventory(balance);
			}else if (choice.equals("3")){
				System.exit(0);
			}else{
				JOptionPane.showMessageDialog(null, "Invalid input");
				loop = true;
			}
		} while (loop);
		
	}
	
	/***
	 * Manage Dealership's inventory and execute transactions
	 */
	public static void manageInventory(Double capital){
		FamilyCar[] fcars = new FamilyCar[10];
		String[][] info = new String[3][10];
		try {
			BufferedReader reader =new BufferedReader(new FileReader("src/lab3/inventory.csv"));
			int row = 0;
			String line = reader.readLine();	// skip header
			while((line=reader.readLine())!=null){
				String [] values =line.trim().split(",");
                FamilyCar fc = new FamilyCar(values[0], values[1], Integer.parseInt(values[2]), values[3], 
                		Double.parseDouble(values[4]), Double.parseDouble(values[5]), Double.parseDouble(values[6]));
                fcars[row] = fc;
                info[0][row] = values[7];
                info[1][row] = values[8];
                row++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double balance = capital;
		double cost;
		int sell=0, buy=0, pass=0;
		for (int c=0; c<fcars.length; c++){
			if (info[1][c].equals("Sell")){
				cost = getCostPrice(fcars[c], Integer.parseInt(info[0][c]));
				balance += cost + (cost*0.15);
				info[2][c] = "remove";
				sell += 1;
			} else if (info[1][c].equals("Buy")){
				cost = getCostPrice(fcars[c], Integer.parseInt(info[0][c]));
				if (cost <= balance){
					balance -= cost;
					info[2][c] = "add";
					buy += 1;
				} else {
					pass += 1;
					info[2][c] = "remove";
				}
			}
		}
		
		
		String summary = "Vehicles still owned by dealership after transactions are:\n";
		for (int c=0; c<fcars.length; c++){
			if (info[2][c].equals("add"))
				summary += fcars[c].toString()+"\n";
		}
		summary += sell+"  cars were sold by dealership\n";
		summary += buy+"  cars were purchased by dealership\n";
		summary += pass+"  deals were missed by dealership\n";
		summary += "Dealership's account balance after transactions is $"+dec.format(balance);
		summary += "\nDealership made a profit of $"+dec.format(balance-capital);
		JOptionPane.showMessageDialog(null,summary);
        
	}
	
	/***
	 * computes the cost price of a vehicle
	 * @param fc: FamilyCar object
	 * @param value: net value of vehicle
	 * @return value: computed gross value of vehicle
	 */
	public static double getCostPrice(FamilyCar fc, int value){
		if (fc.getColor().equalsIgnoreCase("Black"))
			value += 700;
		if (fc.getColor().equalsIgnoreCase("White"))
			value += 1000;
		value += 5000/(2018-fc.getYear());
		value += 49*fc.getTrunkcap();
		return value;
	}
	
	/***
	 * Route vehicles
	 */
	public static void routeVehicles(DumpTruck dtk, SchoolBus sbs){
		JFrame frame = new JFrame("InputDialog1");	
		int dtStart = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to start "
				+ "location of Dump Truck\n"+ displayNodeOptions(-1, -1, -1)));
		int dtEnd = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to destination "
				+ "of Dump Truck\n"+ displayNodeOptions(dtStart, -1, -1)));
		int sbStart = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to start "
				+ "location of School Bus\n"+ displayNodeOptions(dtStart, dtEnd,-1)));
		int sbEnd = Integer.parseInt(JOptionPane.showInputDialog(frame, "enter number corresponding to destination "
				+ "of School Bus\n"+ displayNodeOptions(dtStart, dtEnd, sbStart)));
		String[] dtRoutes = getRoutes(dtStart,dtEnd);
		String[] sbRoutes = getRoutes(sbStart,sbEnd);
		String dtRoute = getShortRoute(dtRoutes);
		String sbRoute = getShortRoute(sbRoutes);
		boolean collision = false;
		char[] junctions = {'I','J','K','L'};
		
		do {
			// check for collision when both routes cross the same junction
			collision = false;
			
			for (char junction : junctions ){
				if (dtRoute.indexOf(junction)>=0 && sbRoute.indexOf(junction)>=0){ 
					int dtMilesToJunction = getMiles(dtStart-1, junction, dtRoute);
					int sbMilesToJunction = getMiles(sbStart-1, junction, sbRoute);
					double dtTime = (float)dtMilesToJunction/(float)dtk.getSpeed();
					double sbTime = (float)sbMilesToJunction/(float)sbs.getSpeed();
					if (dtTime == sbTime)
						collision = true;
				}
			}
			// reroute or change vehicle speed if collision
			if (collision){
				boolean repeat = false;
				do {		
					String choice = JOptionPane.showInputDialog(frame, "A collision is pending, avoid collision by:\n"
							+ "1. Rerouting a vehicle\n"
							+ "2. Change the speed of a vehicle");
					String vehicle = JOptionPane.showInputDialog(frame, "Choose vehicle to control:\n"
							+ "1. Dump Truck\n"
							+ "2. School Bus");
					if (choice == "1"){
						if (vehicle == "1"){
							for (String route : dtRoutes){
								if (route != dtRoute)
									dtRoute = route;
								else repeat = true;
							}
						} else if (vehicle == "2"){
							for (String route : sbRoutes){
								if (route != sbRoute)
									sbRoute = route;
								else repeat = true;
							}
						}
					} else if (choice == "2"){
						double speed = Double.parseDouble(JOptionPane.showInputDialog(frame, "Enter new average speed"));
						if (vehicle == "1")
							dtk.setSpeed(speed);
						else if (vehicle == "2")
							sbs.setSpeed(speed);
					}
				} while (repeat);
			} else{
				int sbMiles = getMiles(sbRoute);
				int dtMiles = getMiles(dtRoute);
				String summary = "School Bus route is: "+sbRoute
						+"\nTotal distance traveled is: "+sbMiles+" Miles"
						+"\nAverage speed during trip is: "+sbs.getSpeed()+" MPH"
						+"\nTotal time of trip is: "+dec.format(60*(sbMiles/sbs.getSpeed()))+" minutes"
						+"\nGas burned during trip is: "+dec.format(sbMiles/sbs.getMpg())+" gallons\n"
						+"\nDump Truck route is: "+dtRoute
						+"\nTotal distance traveled is: "+dtMiles+" Miles"
						+"\nAverage speed during trip is: "+dtk.getSpeed()+" MPH"
						+"\nTotal time of trip is: "+dec.format(60*(dtMiles/dtk.getSpeed()))+" minutes"
						+"\nGas burned during trip is: "+dec.format(dtMiles/dtk.getMpg())+" gallons\n";
				JOptionPane.showMessageDialog(null,summary);
			}
		} while (collision);
	}
	
	public static int getMiles(String route){
		int miles = 0;
		char[] rNodes = route.toCharArray();
		for (int c=0; c<rNodes.length-1; c++){
			miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
		}
		return miles;
	}
	
	public static int getMiles(int sin, char junc, String route){
		int miles = 0;
		char[] rNodes = route.toCharArray();
		if (sin == convertToInt(route.charAt(0))){
			for (int c=0; c<route.indexOf(junc)-1; c++){
				miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
			}
		} else {
			for (int c=route.indexOf(junc); c<route.length()-1; c++){
				miles += adjMatrix[convertToInt(rNodes[c])][convertToInt(rNodes[c+1])];
			}
		}
		return miles;
	}
	
	/***
	 * evaluates and returns the route with shortest distance
	 * @param routes: array of different routes from source to sink
	 * @return argmin: shortest route
	 */
	public static String getShortRoute(String[] routes){
		int min = 50000;
		String argmin = " ";
		int miles;
		for (String route : routes){
			if (route != null){
				miles = getMiles(route);
				if (miles<min){
					min = miles;
					argmin = route;
				}
			}
		}
		return argmin;
	}
	
	/***
	 * searches dictionary to find routes
	 * @param sin, ein are start and end nodes for route
	 * @return array of all routes found
	 */
	public static String[] getRoutes(int sin, int ein){
		String[] routes = new String[2];
		String t1 = convertToString(sin-1)+"-"+convertToString(ein-1);
		String t2 = convertToString(ein-1)+"-"+convertToString(sin-1);
		for (int i=0; i<routeDict.length; i++){
			if (t1.equals(routeDict[i][0]) || t2.equals(routeDict[i][0])){
				routes[0]=routeDict[i][1];
				routes[1]=routeDict[i][2];
			}
		}
		return routes;
	}
	
	/***
	 * Converts node index to alphabet
	 * @param node: integer representing node
	 * @return alphabet corresponding to node
	 */
	public static String convertToString(int node){
		switch (node){
		case 0: return "A";
		case 1: return "B";
		case 2: return "C";
		case 3: return "D";
		case 4: return "E";
		case 5: return "F";
		case 6: return "G";
		case 7: return "H";
		case 8: return "I";
		case 9: return "J";
		case 10: return "K";
		case 11: return "L";
		}
		return " ";
	}
	
	/***
	 * Converts node alphabet to integer
	 * @param node: alphabet representing node
	 * @return integer corresponding to node
	 */
	public static int convertToInt(char node){
		switch (node){
		case 'A': return 0;
		case 'B': return 1;
		case 'C': return 2;
		case 'D': return 3;
		case 'E': return 4;
		case 'F': return 5;
		case 'G': return 6;
		case 'H': return 7;
		case 'I': return 8;
		case 'J': return 9;
		case 'K': return 10;
		case 'L': return 11;
		}
		return -1;
	}
	
	/***
	 * lists only nodes that haven't yet been chosen
	 * @param node1, node2, node3 are already chosen nodes
	 * @return string listing available options
	 */
	public static String displayNodeOptions(int node1, int node2, int node3){
		String options = "";
		if (node1 != 1 && node2 != 1 && node3 != 1)
			options += "1. A\n";
		if (node1 != 2 && node2 != 2 && node3 != 2)
			options += "2. B\n";
		if (node1 != 3 && node2 != 3 && node3 != 3)
			options += "3. C\n";
		if (node1 != 4 && node2 != 4 && node3 != 4)
			options += "4. D\n";
		if (node1 != 5 && node2 != 5 && node3 != 5)
			options += "5. E\n";
		if (node1 != 6 && node2 != 6 && node3 != 6)
			options += "6. F\n";
		if (node1 != 7 && node2 != 7 && node3 != 7)
			options += "7. G\n";
		if (node1 != 8 && node2 != 8 && node3 != 8)
			options += "8. H";
		return options;
	}
	
	/***
	 * Read data from text files using buffered Reader and also prints content to console
	 * @param filepath: path to text file
	 */
	public static void copyTextFileIntoArray(String filepath) {
		try {		
			Scanner line = new Scanner(new File(filepath));
			int row = 0;
			int col;
			    
		    while (line.hasNextLine()) {
		    	col = 0;
		        Scanner word = new Scanner(line.nextLine());
		        while (word.hasNext()) {
		        	String s = word.next();
		        	adjMatrix[row][col] = Integer.parseInt(s);
		        	System.out.print(adjMatrix[row][col]+"\t");
		            col++;
		        }
		        row++;
		        System.out.println();
		        word.close();
		    }
		    line.close();			    
				    
	    } catch (IOException e) {
	    	e.printStackTrace();
		}
	}
	
	/***
	 * Read data from text files using buffered Reader
	 * @param filepath: path to text file
	 */
	public static void buildDictionary(String filepath) {
		try {		
			Scanner line = new Scanner(new File(filepath));
			int row = 0;
			int col = 0;
			    
		    while (line.hasNextLine()) {
		    	col = 0;
		        Scanner word = new Scanner(line.nextLine());
		        while (word.hasNext()) {
		        	String s = word.next();
		        	routeDict[row][col] = s;
		        	System.out.print(routeDict[row][col]+"\t");
		            col++;
		        }
		        row++;
		        System.out.println();
		        word.close();
		    }
		    line.close();			    
	    } catch (IOException e) {
	    	e.printStackTrace();
		}
	}
}
