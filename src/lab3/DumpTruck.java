package lab3;

/**
 * @author Lawrence
 * This class implements an object of DumpTruck Vehicle
 */
public class DumpTruck {
	private String make;		//make of the vehicle
	private String model;		//model of the vehicle
	private int year;			//year of vehicle
	private String color;		//vehicle color
	private double mpg;			//vehicle gas consumption rate in miles per gallon
	private double loadcap;		//vehicle load capacity in cubic yards
	private double speed;		//average speed of vehicle in miles per hour
	
	public DumpTruck() {
		setMake("Some");
		setModel("Vehicle");
		setYear(3333);
		setColor("Pink");
		setMpg(0);
		setLoadcap(0);
		setSpeed(0);
	}
	
	public DumpTruck(String mk, String md, int yr, String clr, double grate, double lcap, double spd) {
		setMake(mk);
		setModel(md);
		setYear(yr);
		setColor(clr);
		setMpg(grate);
		setLoadcap(lcap);
		setSpeed(spd);
	}

	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}

	/**
	 * @param make the make to set
	 */
	public void setMake(String make) {
		this.make = make;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the gasrate
	 */
	public double getMpg() {
		return mpg;
	}

	/**
	 * @param gasrate the gasrate to set
	 */
	public void setMpg(double gasrate) {
		this.mpg = gasrate;
	}

	/**
	 * @return the loadcap
	 */
	public double getLoadcap() {
		return loadcap;
	}

	/**
	 * @param loadcap the loadcap to set
	 */
	public void setLoadcap(double loadcap) {
		this.loadcap = loadcap;
	}
	
	/**
	 * @return the speed
	 */
	public double getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	/**
	 * @return the String of all fields
	 */
	public String toString(){
		return getColor()+" "+getYear()+" "+getMake()+" "+getModel()+
				" with an average MPG of "+getMpg()+" miles per gallon and a load capacity of "
				+getLoadcap()+" cubic yards, and average speed of " +getSpeed()+" MPH";
	}
	
	/**
	 * @param distance: the distance in miles traveled by vehicle
	 * @return the amount of gas consumed in liters
	 */
	public double calcGasUsed(double distance){
		double gallons = distance/getMpg();
		double liters = gallons*3.785;
		return liters;
	}
	
	/**
	 * @param vehicle: object of some dump truck vehicle
	 * @return true if objects are a match, false otherwise
	 */
	public boolean equals(DumpTruck vehicle){
		if (this.getMake().equals(vehicle.getMake()) && this.getModel().equals(vehicle.getModel())
				&& this.getYear()==vehicle.getYear() && this.getColor() == vehicle.getColor())
				return true;
		return false;
	}
}
