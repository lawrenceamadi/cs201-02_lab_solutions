package lab2;

import java.util.Scanner;

/**
 * @author Lawrence
 * Build, run, and test implementations of DumpTruck.java, SchoolBus.java, FamilyCar.java
 */
public class TestVehicle {

	public static void main(String[] args) {
		// Instantiate 3 vehicle objects
		DumpTruck dt = new DumpTruck("Mack", "Granite", 2017, "White", 5.5, 13, 55);
		SchoolBus sb = new SchoolBus("Blue Bird", "Conventional", 2012, "Yellow", 11.2, 24, 55);
		FamilyCar fc1 = new FamilyCar("Toyota", "Sienna", 2008, "Blue", 19, 38.4, 65);
		FamilyCar fc2 = new FamilyCar("Honda", "Odyssey", 2015, "Black", 19.7, 40, 70);
		
		// Use toString() to display object attributes
		System.out.println(dt.toString());
		System.out.println(sb.toString());
		System.out.println(fc1.toString());
		System.out.println(fc2.toString());
		
		// Compare both family cars
		if (fc1.equals(fc2))
			System.out.println("\nVehicle 1 and Vehicle 2 are similar\n");
		else
			System.out.println("\nVehicle 1 and Vehicle 2 are different\n");
		
		char st1, st2, ds1, ds2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Inputs are either N, S, E, or W\nEnter start location of vehicle 1");
		st1 = sc.next().charAt(0);
		System.out.println("Enter destination of vehicle 1");
		ds1 = sc.next().charAt(0);
		System.out.println("Enter start location of vehicle 2");
		st2 = sc.next().charAt(0);
		System.out.println("Enter destination of vehicle 2");
		ds2 = sc.next().charAt(0);
		
		boolean repeat = collisionGame(fc1, fc2, st1, st2, ds1, ds2);
		while (repeat){
			int choice; double newAvgSpeed;
			System.out.println("Change the average speed of one of the vehicles to avoid collision\n"
					+ "Enter 1, to change speed of vehicle 1\n"
					+ "Enter 2, to change speed of vehicle 2");
			choice = sc.nextInt();
			System.out.println("Enter new average speed of the vehicle");
			newAvgSpeed = sc.nextDouble();
			
			if (choice == 1)
				fc1.setSpeed(newAvgSpeed);
			if (choice == 2)
				fc2.setSpeed(newAvgSpeed);
			
			repeat = collisionGame(fc1, fc2, st1, st2, ds1, ds2);
		}
	}
	
	/**
	 * Collision game detects when a collision is eminent.
	 * It is important to note that loc1 != loc2 != dst1 != dst2
	 * or the intersection of both start locations and both destination is an empty set
	 * @param fc1: 1st family car object
	 * @param fc2: 2nd family car object
	 * @param loc1: start location of 1st family car
	 * @param loc2: start location of 2nd family car
	 * @param dst1: destination of 1st family car
	 * @param dst2: destination of 2nd family car
	 * @return true if a collision is eminent, false otherwise
	 */
	public static boolean collisionGame(FamilyCar fc1, FamilyCar fc2, char loc1, char loc2, char dst1, char dst2){
		// Notice from the assumptions made in the lab description a collision can only occur at the junction (0 miles)
		// Therefore, by checking if both vehicles arrive at the junction at the same time is sufficient for a collision test
		boolean collision = false;
		double t1, t2;	// time in hrs it takes vehicle 1 & 2 to arrive at junction
		int d1, d2;		// distance in miles from loc1 & loc2 to junction
		d1 = getDistanceToJunction(loc1);
		d2 = getDistanceToJunction(loc2);
		t1 = d1/fc1.getSpeed();
		t2 = d2/fc2.getSpeed();
		if (t1 == t2) {
			System.out.println("A collision is eminent at junction\n"
					+ "It will happen "+t1+" hours from start time");
			collision = true;
		} else
			System.out.println("Congratulations! No collision is eminent");
		
		return collision;
	}
	
	/**
	 * @param cardPoint: one of the four points N, S, E, W
	 * @return the distance in miles from the point to the junction
	 */
	private static int getDistanceToJunction(char cardPoint) {
		switch (cardPoint){
		case 'N': case 'S': return 15;
		case 'E': case 'W': return 25;
		}
		return 0;
	}

}
