package lab1;

/**
 * @author Lawrence
 * Build, run, and test implementations of DumpTruck.java, SchoolBus.java, FamilyCar.java
 */
public class TestVehicle {

	public static void main(String[] args) {
		// Instantiate 3 vehicle objects
		DumpTruck dt = new DumpTruck("Mack", "Granite", 2017, "White", 5.5, 13);
		SchoolBus sb = new SchoolBus("Blue Bird", "Conventional", 2012, "Yellow", 11.2, 24);
		FamilyCar fc = new FamilyCar("Toyota", "Sienna", 2008, "Blue", 19, 38.4);
		
		// Use toString() to display object attributes
		System.out.println(dt.toString());
		System.out.println(sb.toString());
		System.out.println(fc.toString()+"\n");
		
		// Calculate and display the amount of gas used by each vehicle
		System.out.println(dt.getMake()+" "+dt.getModel()+" travelled 15 miles and used "+dt.calcGasUsed(15)+" liters of gas");
		System.out.println(sb.getMake()+" "+sb.getModel()+" travelled 63 miles and used "+sb.calcGasUsed(63)+" liters of gas");
		System.out.println(fc.getMake()+" "+fc.getModel()+" travelled 58 miles and used "+fc.calcGasUsed(58)+" liters of gas");
	}

}
