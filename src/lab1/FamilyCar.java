package lab1;

/**
 * @author Lawrence
 * This class implements an object of FamilyCar Vehicle
 */
public class FamilyCar {
	private String make;		//make of the vehicle
	private String model;		//model of the vehicle
	private int year;			//year of vehicle
	private String color;		//vehicle color
	private double mpg;			//vehicle gas consumption rate in miles per gallon
	private double trunk;		//vehicle's trunk size in cubic feet
	
	public FamilyCar() {
		setMake("Some");
		setModel("Vehicle");
		setYear(3333);
		setColor("Pink");
		setMpg(0);
		setTrunkcap(0);
	}
	
	public FamilyCar(String mk, String md, int yr, String clr, double grate, double trunk) {
		setMake(mk);
		setModel(md);
		setYear(yr);
		setColor(clr);
		setMpg(grate);
		setTrunkcap(trunk);
	}

	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}

	/**
	 * @param make the make to set
	 */
	public void setMake(String make) {
		this.make = make;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the gasrate
	 */
	public double getMpg() {
		return mpg;
	}

	/**
	 * @param gasrate the gasrate to set
	 */
	public void setMpg(double gasrate) {
		this.mpg = gasrate;
	}

	/**
	 * @return the trunk size
	 */
	public double getTrunkcap() {
		return trunk;
	}

	/**
	 * @param trunk the trunk to set
	 */
	public void setTrunkcap(double trunk) {
		this.trunk = trunk;
	}
	
	/**
	 * @return the String of all fields
	 */
	public String toString(){
		return "The vehicle is a "+getColor()+" "+getYear()+" "+getMake()+" "+getModel()+
				" with an average MPG of "+getMpg()+" miles per gallon and a trunk size of "
				+getTrunkcap()+" cubic feet";
	}
	
	/**
	 * @param distance: the distance in miles traveled by vehicle
	 * @return the amount of gas consumed in liters
	 */
	public double calcGasUsed(double distance){
		double gallons = distance/getMpg();
		double liters = gallons*3.785;
		return liters;
	}
}
